var win=window,
	screenObj=win.screen,
	doc=win.document,
	bodyObj=doc.body;

var defaultTpl=['<div class="vue-position-container" :style="position" v-show="isShow"><slot></slot></div>'];


var offsets={
	"top":"offsetTop",
	"left":"offsetLeft",
	"width":"offsetWidth",
	"height":"offsetHeight"
};


function getOffset(el,key){
	return el[offsets[key]]?el[offsets[key]]:"";
};


function hasDirection(align){
	return align||align===""?false:true;
};


function setPosition(self,targetElement){
	var offset={},
		el=self.$el,
		align=self.align,
		target=targetElement;

	var width=getOffset(self.$el,"width");
	var height=getOffset(self.$el,"height");
	var top=getOffset(target,"top");
	var left=getOffset(target,"left");

	var pos_direction=["tl","tc","tr","bl","bc","br","lc","rc"];


	switch(align){
		//top-left
		case pos_direction[0]:updatePosition(self,{
				"top":top-height,
				"left":left-width
			});
			break;
		//top-center	
		case pos_direction[1]:updatePosition(self,{
				"top":top+height/2,
				"left":left
			});
			break;
		//top-right		
		case pos_direction[2]:updatePosition(self,{
				"top":top,
				"left":left+width
			});
			break;
		//bottom-left		
		case pos_direction[3]:updatePosition(self,{
				"top":top+height/2,
				"left":left
			});
			break;
		//bottom-center		
		case pos_direction[4]:updatePosition(self,{
				"top":top+height/2,
				"left":left
			});
			break;
		//bottom-right		
		case pos_direction[5]:updatePosition(self,{
				"top":top+height/2,
				"left":left
			});
			break;
		//left-center	
		case pos_direction[6]:updatePosition(self,{
				"top":top+height/2,
				"left":left
			});
			break;
		//right-center
		case pos_direction[7]:updatePosition(self,{
				"top":top+height/2,
				"left":left
			});
			break;
	}

};


function updatePosition(self,pos){
	self.position={
		"top":pos['top']+"px",
		"left":pos['left']+"px"
	};
};

// var Position=function(){};
// Position.prototype={

// }

var PositionContainer={
	template:defaultTpl.join(""),
	props:{
		position:Object,
		align:String,
		offset:Array,
		autoHide:Boolean	
	},
	data:function(){
		return {
			"align":"bl",
			"offset":[0,0],
			"autoHide":true,
			"isShow":false,
			"position":{
				"top":null,
				"left":null
			}
		}
	},
	ready:function(){
	},
	methods:{
		position:function(){
			var self=this;
			return self.position;
		},
		top:function(){
			var self=this;
			return self.position.top;
		},
		left:function(){
			var self=this;
			return self.position.left;
		},
		show:function(target){
			var self=this;
			this.isShow=true;
			setPosition(self,target);
		},
		hide:function(){
			this.isShow=false;
		},
		update:function(target){
			var self=this;
			setPosition(self,target);
		}
	}
};

module.exports= PositionContainer;

