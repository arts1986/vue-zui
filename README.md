## vui

### 移动端UI框架，基于Vue.js实现。

持续更新移动webapp项目所需的常用组件

目前是beta版本,有些规范还在优化中,升级时请注意留意文档更新说明

### 更新至beta 1.0.3
![输入图片说明](http://git.oschina.net/uploads/images/2016/0919/225759_aeedc6e4_331468.png "vue-zui")

## Browser
	
        <link href="../dist/vui.min.css" rel="stylesheet">
        <script src="../dist/vue.js"></script>
        <script src="../dist/vui.min.js"></script>
        <script>
        var Toast= vui.Toast
        var app = new Vue({
            el : '#app',
            components:{
 		Toast
 	    }
        });
        </script>

## Modules
        
	<Alert 
	:visible.sync="alert.visible"
	:transition="alert.transition"
	:content="alert.content"
	@ok="onOk"
	></Alert>

	import Alert from "components/dialog/alert"
	

## 安装

npm install vue-zui 

## webpack 构建

npm run build

## 启动本地服务

npm run dev

http://localhost:9092/index.html#!/